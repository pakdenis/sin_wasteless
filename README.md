# sin_wasteless
## Project description
Market place for restaurants and other food sellers to offer food that's about to expire with discount.

Our aim is to reduce unnecessory food waste and increase profits for food providers.

## Navigation
### Documentation
All versions of project documentation as it was created.
### Presentation
Short presentations for labs.
### Review
Reviews for other teams.

## Contacts
team email: teamwasteless@googlegroups.com

For personal emails see "Participants"
## Participants
 - Batuev Egor (batueego@fel.cvut.cz)
 - Pak Denis (pakdenis@fel.cvut.cz)
 - Aubrecht Petr (aubrepet@fel.cvut.cz)
 - Shvaibovich Ales (shvaiale@fel.cvut.cz)
 - Liamkin Artem (liamkart@fel.cvut.cz)
 